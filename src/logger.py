import logging
from pathlib import PurePath
from logging.handlers import RotatingFileHandler


class Logger():
    @property
    def logger(self):
        name = 'media_converter.log'
        lg = logging.getLogger(name)
        lg.setLevel(10)
        formatter = logging.Formatter(
            '%(name)s %(asctime)s %(levelname)s \
[%(module)s:%(lineno)s]: %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')
        if not lg.hasHandlers():
            # logging to file
            path = PurePath('.', name)
            handler = RotatingFileHandler(
                str(path),
                maxBytes=10000000,
                backupCount=3)
            handler.setLevel(10)
            handler.setFormatter(formatter)
            lg.addHandler(handler)

            # logging to console
            handler = logging.StreamHandler()
            handler.setLevel(10)
            handler.setFormatter(formatter)
            lg.addHandler(handler)
        return lg
