
import os
import json
import argparse
from datetime import datetime
import pyheif
import piexif
from PIL import Image
from logger import Logger


class Converter(Logger):
    def __init__(self):
        self.args = self.get_input_args()
        self.logger.debug(f"Input arguments: {self.args}")
        # Check whether input and output folders exist
        if (not os.path.isdir(
            self.args.input_path) or not os.path.isdir(
                self.args.output_path)):
            raise Exception("Input or output folder does not exist")
        input_ext = self.args.inp_ext.lower().split(',')
        self.logger.debug(f"Input extensions: {input_ext}")
        self.walk_path(input_ext)

    def get_input_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--inp-ext",
            help="Input file extension. The defailt is 'HEIC'. \
                 Accepts comma-separated list of file extentions.",
            default='heic')
        parser.add_argument(
            "--out-ext",
            help="Output file extension. The defailt is 'JPEG'",
            default='jpeg')
        parser.add_argument(
            "input_path",
            help="Path to the files we want to convert")
        parser.add_argument(
            "output_path",
            help="Path to a directory where we want \
                 the converted files to be saved")
        return parser.parse_args()

    def walk_path(self, allowed_ext: list):
        for root, dirs, files in os.walk(self.args.input_path):
            for fil in files:
                extension = fil.split(".")[-1].lower()
                if extension not in allowed_ext:
                    continue
                self.parse_file(root, fil)

    def parse_file(self, root, fil):
        path = os.path.join(root, fil)
        self.logger.debug(f"Parsing file {path}")
        heif_file = pyheif.read(path)
        # Creation of image
        image = Image.frombytes(
            heif_file.mode,
            heif_file.size,
            heif_file.data,
            "raw",
            heif_file.mode,
            heif_file.stride,
        )
        # Retrive the metadata
        for metadata in heif_file.metadata or []:
            if metadata['type'] == 'Exif':
                exif_dict = piexif.load(metadata['data'])

        # PIL rotates the image according to exif info, so it's necessary
        # to remove the orientation tag otherwise the image will be rotated
        # again (1° time from PIL, 2° from viewer).
        exif_dict['0th'][274] = 0
        datetime_object = datetime.strptime(
            exif_dict['0th'][306].decode(), '%Y:%m:%d %H:%M:%S')
        dts = datetime.strftime(datetime_object, '%Y%m%d_%H%M')
        exif_bytes = piexif.dump(exif_dict)
        fn = f'{dts}.{fil.split(".")[0].lower()}.jpeg'.upper()
        out_path = os.path.join(self.args.output_path, fn)
        image.save(out_path, "JPEG", exif=exif_bytes, quality=100)


if __name__ == "__main__":
    cv = Converter()
